PROJ = amgen

DATA_PATH = data/
WWW_PATH = www/

DATA = script.python/$(PROJ).csv data/$(PROJ).RData

all : $(DATA) $(WWW)

### Generació python
script.python/$(PROJ).csv : script.python/generation.py
	python script.python/generation.py > $(WWW_PATH)generation.txt

### Inicialització
data/$(PROJ).RData : script.R/run-knit2html.R script.R/01-initialize.Rmd script.python/$(PROJ).csv
	mkdir -p .tmp/$@ 
	Rscript -e 'OUT = ".tmp/$@/01-initialize.html"; IN = "script.R/01-initialize.Rmd"; source("script.R/run-knit2html.R")'
	mv .tmp/$@/01-initialize.html www/


