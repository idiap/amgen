# -*- coding: utf-8 -*-

##
## Canvis a 19-03-2014:   
## es refà la versió JAMA
## s'agafa la primera mesura d'itb --> sorted(list(itb[i]))

from nova2.population import Population
from nova2.problems import Problems
from nova2.treatment import Treatment
from nova2.cohort import Cohort
from nova2.variable import Variable
from nova2.visits import Visits
from nova2.eq import EQ
from nova2.paths import Paths
from utils import misc
import datetime

begin_study = datetime.date(2006,1,1)
end_study = datetime.date(2012,12,31)
stat_restart = datetime.date(2007,1,1)
paths = Paths('2013')

# Initial population
population = Population(paths.get('sidiapq'), cmbd_path=paths.get('cmbd'))

problems = Problems(paths)

cohort = Cohort(population = population, 
                beginning = begin_study, 
                ending = end_study, 
                minIntro = begin_study, 
                maxIntro = begin_study)

cohort.setStatusFilter(population)

cohort.setIntro()

cohort.addData(cohort.cohortIntro, 'dintro')
cohort.addData(population.ageAt(cohort.cohortIntro), 'age')
cohort.addData(population.sex(cohort.cohort), 'sex')

cohort.addData(cohort._exitus_reason, 'exitus')
cohort.addData(cohort._exitus, 'dexitus')

l_antecedents = ['ami_anni', 'angor', 'stroke_i', 'stroke_e', 'stroke_h', 'pad', 'tia', 'ihd_acute', 'ihd_chronic',
                 'gangrene', 'amputation']
l_procedures = ['card_proc']

ev = problems.get_events(cohort.cohort, l_antecedents)
pr = problems.get_procedures(cohort.cohort, l_procedures)

cardio_event = misc.getMinDate(problems.get_absolute_minimum(ev), problems.get_absolute_minimum(pr))

cv_ant = set([o for o in cardio_event if cardio_event[o] <= begin_study])

cohort.addBinaryData( cv_ant, 'cv_ant' )

## Reiniciaments
statines = Treatment(population = cohort.cohort, filename = paths.treatment('statine'), catalog = paths.get('catalog') )
first_ending = statines.firstEnding(cohort.cohort, months = 6, after = cohort.cohortIntro, before=stat_restart)

restart = statines.firstBeginning(first_ending, months = 6, after = first_ending, before=stat_restart)
valid_restart =set([o for o in restart if o not in cardio_event or restart[o] <= cardio_event[o]])
cohort.addBinaryData(valid_restart, 'stat_restart' )

stat2006 = statines.firstFacturation(cohort.cohort, after=datetime.date(2005, 12, 31), before=datetime.date(2007, 1, 1))
cohort.addData(stat2006, 'stat06')
stat2007 = statines.firstFacturation(cohort.cohort, after=datetime.date(2006, 12, 31), before=datetime.date(2008, 1, 1))
cohort.addData(stat2007, 'stat07')

hard_ep = ['ami', 'angor', 'stroke_i', 'stroke_e', 'tia', 'pad']
### all
evts = problems.get_events(cohort.cohort, hard_ep, after = cohort.cohortIntro, before = cohort._exitus)
cohort.addData(problems.get_relative(evts, hard_ep, which=0), ['ep_' + i for i in hard_ep], default_na = 'NA')

# Problem incidence (cmbd)
evts = problems.get_events(cohort.cohort, hard_ep, after = cohort.cohortIntro, before = cohort._exitus, use_ecap = False)
cohort.addData(problems.get_relative(evts, hard_ep, which=0), ['ep_' + i + '.cmbd' for i in hard_ep], default_na = 'NA')

# Problem incidence (ecap)
evts = problems.get_events(cohort.cohort, hard_ep, after = cohort.cohortIntro, before = cohort._exitus, use_cmbd = False)
cohort.addData(problems.get_relative(evts, hard_ep, which=0), ['ep_' + i + '.ecap' for i in hard_ep], default_na = 'NA')

cohort.writeTable("script.python/amgen.csv")
